#!/usr/bin/env python
# coding: utf-8

# # Build corpus for TXM
# 
# Le but ici est de créer un ensemble de textes à verser sur TXM (en mode XTZ + CSV).
# 
# Les XML TEI d'entrée sont en XML TEI Lite, et contiennent plusieurs fichiers groupés sous la structure `<text><group><text><body>` 
# 
# En sortie, on veut un fichier CSV contenant les métadonnées (id, date de publication / d'audience, titre, journal ou origine, etc.) 
# 

# ## exemple de la structure dans `presse_ouvriere_La-tribune-lyonnaise.xml` 
# 
# ``` xml
# <text>
#     <body xml:id="tribune-lyonnaise_annee-1_1">
#         <date when="1845-03-00">
#         <div>
#             <milestone unit="article"/>
#             <!-- <pb n="3"/> -->
#             <head>CONSEIL DES PRUD'HOMMES</head>
#             <milestone unit="hearing"/>
#             <p>26 février 1845.</p>
#             <p>du texte</p>
#             <p>du texte</p>
#         </div>
#     </body>
# </text>
# ```

# ## exemple de la structure dans `struct_AD75 D1U10 379 Prud'hommes 1847-1849.xml` 
# 
# 
# ``` xml
# <text>
#     <!-- <pb n="4"/> -->
#     <body xml:id="cph-paris-1847-49_1">
#         <milestone unit="case"/>
#         <p>Dudit jour Entre Madame Pauline Loger,...</p>
#     </body>
# </text>
# ```

# In[1]:


# imports
import os
from bs4 import BeautifulSoup as BS


# In[2]:


# source

src_cph_paris = ".../struct_AD75 D1U10 379 Prud'hommes 1847-1849.xml"

src_presse_lyon = ".../presse_ouvriere_La-tribune-lyonnaise.xml"


# In[3]:


# tests

def test_loading(src):
    try:
        content = load_data(src)
        print("successful load!")
    except Exception as e:
        print("failed to load:")
        print(e)
    


# In[4]:


# i/o

PATH = ".../corpus_de_test_txm/"

def load_data(source):
    with open(source, "r") as fh:
        data = fh.read()
    data_parsed = BS(data, "xml")
    return data_parsed


def save_xml_file(filename, content, encoding="utf-8"):
    topline = """<?xml version="1.0" encoding="utf-8"?>"""
    topline_and_schema = """<?xml version="1.0" encoding="utf-8"?>\n<?xml-model href="http://www.tei-c.org/release/xml/tei/custom/schema/relaxng/tei_lite.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>\n<?xml-model href="http://www.tei-c.org/release/xml/tei/custom/schema/relaxng/tei_lite.rng" type="application/xml" schematypens="http://purl.oclc.org/dsdl/schematron"?>"""
    content = content.prettify().replace(topline, topline_and_schema)
    
    with open("{path}/{filename}.xml".format(path=PATH, filename=filename), "w", encoding=encoding) as fh:
        fh.write(content)
        

def add_metadata_to_csv(metadata, encoding="utf-8"):
    with open("{}/metadata.csv".format(PATH), "a+", encoding=encoding) as fh:
        fh.write("\n{}".format(",".join(metadata)))


# In[10]:


# parsing

def create_new_trees(list_of_elements, header):
    new_trees = []
    for element in list_of_elements:
        new_tree = BS("""<TEI xmlns="http://www.tei-c.org/ns/1.0"><placeholder/></TEI>""", "xml")
        new_tree.TEI.placeholder.replace_with(header)
        new_tree.TEI.teiHeader.insert_after(element.extract())
        new_trees.append(new_tree)
    return new_trees


def add_header(tree, filename, date):
    teiheader = BS("""<teiHeader>
    <fileDesc>
    <titleStmt><title>{title}</title></titleStmt>
    <publicationStmt><publisher/></publicationStmt>
    <sourceDesc><p>publication/rédaction : "{date}"</p></sourceDesc>
    </fileDesc>
    </teiHeader>""".format(title = filename, date = date), "xml")
    
    tree.insert(0, teiheader)
    return tree


def get_texts(tree):
    return tree.group.find_all("text")


def get_filename(tree):
    if tree.body.date:
        filename = "{date}_{id}".format(date=tree.body.date.attrs["when"], id=tree.body.attrs["xml:id"])
    else:
        filename = "00_{id}".format(id=tree.body.attrs["xml:id"])
    return filename


def get_date(tree):
    return tree.body.date.attrs["when"]
    


# In[8]:


# main

def main(mode):
    if mode == "cph":
        src = src_cph_paris
    elif mode == "presse":
        src = src_presse_lyon
    else:
        print("incorrect mode, value must be 'presse' or 'cph'")
        src = None
    
    if src:
        #test_loading(src)
        content = load_data(src)
        # let's keep in mind that these "text" tags will be hard to handle because of "bs_object.text" property 
        texts = get_texts(content)
        trees = create_new_trees(texts, header=content.teiHeader)
        for tree in trees:
            filename = get_filename(tree)
            date = get_date(tree)
            if mode == "cph":
                ensemble = "PH Paris"
                cote = "AD75 D1 U10 379"
            elif mode == "presse":
                ensemble = "PH Lyon"
                cote = ""
            tree = add_header(tree, filename, date)
            add_metadata_to_csv([filename, date, ensemble, cote])
            save_xml_file(filename, tree)


# In[11]:


# excecution
main(mode="cph")
main(mode="presse")

