# coding: utf8

import argparse
import os
from bs4 import BeautifulSoup
import sys
import re
import csv

teiTop = '''<?xml version="1.0" encoding="utf-8"?>
<?xml-model href="http://www.tei-c.org/release/xml/tei/custom/schema/relaxng/tei_all.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<?xml-model href="http://www.tei-c.org/release/xml/tei/custom/schema/relaxng/tei_all.rng" type="application/xml" schematypens="http://purl.oclc.org/dsdl/schematron"?>'''


srcDesc = ['publication', 'audience', 'correction','page']
audience = 'audience='
correction = 'correction='
publication = 'publication='
page = 'page='
#fieldnames = ['id','Date publication', 'Date audience', 'Titre', 'Nombre d\'audiences', 'Numéro page', 'Numéro journal', 'coordinatrice projet',  'Editeur.rice', 'Correcteur.rice', 'Date création', 'Date d\'export en TEI']

fieldnames = ["id",'publicationDate', 'audienceDate', 'title', 'audienceNumber', 'pageNumber', 'numJournal', 'coordinator',  'editors', 'corrector', 'creationDate', 'teiExportDate']

descs = []
# This function tests wheter an argumeb=nt is a folder or not
def handle_folder_content(arg):
    print(type(arg))
    for root, dirs, files in os.walk(arg):
        for filename in files:
            if filename.endswith('.xml'):
                with open(os.path.join(arg, filename)) as fh:
                    xml_content = fh.read()
                    parsed_xml_content = BeautifulSoup(xml_content, 'xml')
                    if "police" in arg: handleRapportDePolice(parsed_xml_content)
                    if "fabrique" in arg: handlePresseOuvriere(arg, parsed_xml_content, filename)
                    if "Hommes" in arg: handlePrudHommes(arg, parsed_xml_content, filename)
        break
    with open(arg+'/NewAudiences/metadata.csv', 'w+', encoding="utf-8") as output_file:
        output_file.seek(0)
        output_file.truncate()
        dict_writer = csv.DictWriter(output_file, fieldnames)
        dict_writer.writeheader()
        dict_writer.writerows(descs)

    print('**************************************')

def to_utf8(lst):
    return [unicode(elem).encode('utf-8') for elem in lst]


def handleRapportDePolice(parsedXMLcontent):
    return False

def handlePresseOuvriere(folderName, parsedXMLcontent , fileName):
    for (num, teiContent) in enumerate(parsedXMLcontent.find_all('TEI'), start=1):
        infos = createMetadata(folderName, teiContent.teiHeader, 'fabrique')
        creatAudience(folderName, infos, teiContent )
    print('****************END PRESSE**********************')


def handlePrudHommes(folderName, parsedXMLcontent , fileName):
        teiContent = parsedXMLcontent.find('TEI')
        teiHeader = teiContent.teiHeader
        divs = teiContent.find('body').findAll("div", {"type" : "courtHearing"})
        soup = BeautifulSoup('<TEI xmlns="http://www.tei-c.org/ns/1.0"> </TEI>', 'xml')
        original_tag = soup.TEI
        original_tag.append(teiHeader)
        
        for (i, div) in enumerate(divs, start=1):
            infos = createMetadata(folderName, teiHeader,'Hommes')
            infos['audienceDate'] = div.attrs['corresp']
            infos['id'] = infos['audienceDate']+ '_PRUD_HOMMES'
            textTag = soup.new_tag('text')
            bodyTag = soup.new_tag('body')
            bodyTag.append(div)
            textTag.append(bodyTag)
            original_tag.append(textTag)
            creatAudience(folderName, infos, original_tag)
            textTag.decompose()
        print('****************END PRUD\'HOMMES' + fileName +' **********************')



def creatAudience(folderName, infos, xmlContent):
    os.makedirs(folderName+'NewAudiences', exist_ok=True) 
    with open(os.path.join(folderName+'NewAudiences/', infos['id'] + '.xml'), 'w+') as temp_file:
        temp_file.truncate(0)
        xmlContent = teiTop + str(xmlContent)
        temp_file.write(xmlContent)


#['id','Date publication', 'Date audience', 'Titre', 'Nombre d\'audiences', 'Numéro page', 'Numéro journal', 'coordinatrice projet',  'Editeur.rice', 'Correcteur.rice', 'Date création', 'Date d\'export en TEI']
def createMetadata(folderName, teiHeader, type):
    print(folderName)
    data = {}
    try:
        infos = teiHeader.find('title').contents[0]
        infosTab = infos.split(',')
        print(infosTab)
        respStmt = teiHeader.findAll('respStmt')
        data['coordinator'] = (teiHeader.find('editor').contents[0]).strip()

        editors =[]
        for entry in respStmt:
            editors.append(entry.find('name').contents[0].strip())
        data['editors'] = ','.join(editors)

        if type == 'fabrique':
            data['title'] = infosTab[0].strip()
            numJournal = infosTab[1].strip().split('-')[0]
            if "duplicated" in numJournal:
                numJournal = numJournal.split('_')[0]
                data['numJournal'] = numJournal.strip()
            else:
                data['numJournal'] = numJournal
            sourceDesc = teiHeader.find('sourceDesc').p
            if any(x in sourceDesc.contents[0] for x in srcDesc): 
                #Date de publication (publication=(.*)audience)|publication=(.*)
                result = re.search('('+publication +'(.*)'+ audience +')|'+publication+'(.*)', sourceDesc.contents[0])
                data['publicationDate'] = result.group(3).strip().replace('"', '') if (result.group(3) != None) else result.group(2).strip().replace('"', '')

                #Date d'audience 
                result = re.search('('+ audience +'(.*)'+ correction +')|'+ audience +'(.*)', sourceDesc.contents[0])
                data['audienceDate'] = result.group(3).strip().replace('"', '') if (result.group(3) != None) else result.group(2).strip().replace('"', '')
                if(data['audienceDate']== ''): 
                    data['audienceDate'] =  re.sub(r"(.*)(\/)(.*)(\/)(.*)", "00\\2\\3\\4\\5", data['publicationDate'].split(',')[0], 0, re.MULTILINE)
                data['audienceNumber'] = len(data['audienceDate'].split(','))

                #Corrector
                result = re.search('('+ correction +'(.*)'+ page +')|'+ correction +'(.*)', sourceDesc.contents[0])
                data['corrector'] = result.group(3).strip().replace('"', '') if (result.group(3) != None) else result.group(2).strip().replace('"', '')

                #Page Number page=(.*)$
                result = re.search(page +'(.*)', sourceDesc.contents[0])
                data['pageNumber'] = result.group(1).strip().replace('"', '')
            else:
                print("Error Error Error Error Error Error Error ")
            data['id']= data['audienceDate'].split(',')[0].replace('/', '-') + '_(' + str(data['audienceNumber']) + ')_' + data['title'] + '_' + str(data['numJournal'])
            data['id'] = urlify(data['id'].strip())
            print(data['id'])
            descs.append(data)
        #Date création
        dates =  teiHeader.find('revisionDesc').findChildren(recursive=False)
        if (len(dates) ==3 ): 
                data['creationDate'] = dates[0].contents[0].split('T')[0].strip()
        #Date d\'export en TEI
                data['teiExportDate'] = dates[2].contents[0].split('T')[0].strip()
        
            

    except:
        print("Oops!",sys.exc_info(),"occured.")
        print()
    return data

def urlify(s):

    # Remove all non-word characters (everything except numbers and letters)
    #s = re.sub(r"[^\w\s]", '', s)

    # Replace all runs of whitespace with a single dash
    s = re.sub(r"\s+", '_', s)
    s = re.sub(r"L'", '', s)
    s = re.sub(r"é", 'e', s)

    return s

def main():
    parser = argparse.ArgumentParser(description="Transform Time Us TEI files to fit well in TXM")
    parser.add_argument('-i', '--input', action='store', nargs='+', required=True, help='Path to directory with files to transform')
    args = parser.parse_args()
    for arg in args.input:
        #test wheter  argument is a folder or not
        if os.path.isdir(arg):
            handle_folder_content(arg)
        else:
            print('on ne gère point')



if __name__ == '__main__':
    main()