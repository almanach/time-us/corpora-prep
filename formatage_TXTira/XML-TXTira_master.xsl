<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl = "http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:xi="http://www.w3.org/2001/XInclude"
    exclude-result-prefixes="xs"
    version="1.0"
    xmlns:tei="http://www.tei-c.org/ns/1.0">
    
    <xsl:output method="text"/> <!-- pour faire une sortie en csv ou en txt -->
    
    <xsl:template match="master_cph-paris"> <!-- xml:id du fichier master -->
        <xsl:apply-templates></xsl:apply-templates>
    </xsl:template>
    
    <xsl:template match="tei:teiHeader"/> <!-- à retirer si l'on veut faire apparaître le teiHeader -->
    <xsl:template match="/"> <!-- appelle la racine de tous les fichiers concernés par le master -->
        <xsl:for-each select="//tei:text">
            <xsl:value-of select=" concat('**** *id_',./@ana,' *type_',.//tei:milestone/@unit,' *ensemble_',.//tei:milestone/@ana,' *date_',.//tei:body//tei:date/@when,' &#xA; ',.//tei:body/tei:head,.//tei:body/tei:p,'&#xA;&#xA;')"/>
        </xsl:for-each>
   </xsl:template>

</xsl:stylesheet>