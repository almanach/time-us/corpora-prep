# Corpora Prep

Ici sont centralisés l'ensemble des scripts de modification des fichiers de texte
pour le projet TIME US.

## Structuration automatique des prud'hommes de Paris et de Lyon (mission de stage de Victoria Le Fourner)
:arrow_right: [Structure_n_Annotate](https://gitlab.inria.fr/almanach/time-us/schema-tei)